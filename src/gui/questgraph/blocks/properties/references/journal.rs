//
// properties::references::journal
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
/// types of journal entry references
#[derive(Debug, PartialEq)]
pub(in gui) enum JournalReferenceType {
    Character,
    Creature,
    Quest,
}
// ----------------------------------------------------------------------------
pub(in gui::questgraph::blocks) struct JournalReference {
    pub(super) label_width: f32,
    pub(super) label: ImString,
    pub(super) r_type: JournalReferenceType,
    pub(super) name: Option<JournalGroup>,
    pub(super) entry: Option<JournalEntry>,
    pub(super) options: Rc<Journals>,
    pub(super) changed: bool,
    pub(super) error: Result<(), ImString>,
}
// ----------------------------------------------------------------------------
pub(in gui::questgraph::blocks) struct JournalQuestOutcomeReference {
    pub(super) label_width: f32,
    pub(super) quest: Option<JournalQuest>,
    pub(super) options: Rc<JournalQuestList>,
    pub(super) changed: bool,
    pub(super) error: Result<(), ImString>,
}
// ----------------------------------------------------------------------------
pub(in gui::questgraph::blocks) struct JournalPhaseObjectivesReference {
    pub(super) label_width: f32,
    pub(super) quest: Option<JournalQuest>,
    pub(super) phase: Option<JournalPhase>,
    pub(super) options: Rc<JournalQuestList>,
    pub(super) changed: bool,
    pub(super) error: Result<(), ImString>,
}
// ----------------------------------------------------------------------------
pub(in gui::questgraph::blocks) struct JournalObjectiveReference {
    pub(super) label_width: f32,
    pub(super) quest: Option<JournalQuest>,
    pub(super) phase: Option<JournalPhase>,
    pub(super) objective: Option<JournalObjective>,
    pub(super) options: Rc<JournalQuestList>,
    pub(super) changed: bool,
    pub(super) error: Result<(), ImString>,
}
// ----------------------------------------------------------------------------
pub(in gui::questgraph::blocks) struct JournalMapPinReference {
    pub(super) label_width: f32,
    pub(super) quest: Option<JournalQuest>,
    pub(super) phase: Option<JournalPhase>,
    pub(super) objective: Option<JournalObjective>,
    pub(super) mappin: Option<JournalMapPin>,
    pub(super) options: Rc<JournalQuestList>,
    pub(super) changed: bool,
    pub(super) error: Result<(), ImString>,
}
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use std::any::Any;
use std::rc::Rc;

use imgui::ImString;

// actions
use super::PropertyAction;

// misc
use super::model;
use super::{AsStr, PropertyControl};

use gui::registry::{
    JournalEntry, JournalGroup, JournalMapPin, JournalObjective, JournalPhase, JournalQuest,
    JournalQuestList, Journals,
};
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
impl JournalReference {
    // ------------------------------------------------------------------------
    pub fn new(
        label_width: f32,
        options: Rc<Journals>,
        reference: Option<&model::JournalElementReference>,
    ) -> JournalReference {
        let (r_type, name, entry) = Self::extract_info(reference);

        JournalReference {
            label_width,
            label: ImString::new(r_type.as_str()),
            r_type,
            name,
            entry,
            options,
            changed: false,
            error: Ok(()),
        }
    }
    // ------------------------------------------------------------------------
    fn extract_info(
        reference: Option<&model::JournalElementReference>,
    ) -> (
        JournalReferenceType,
        Option<JournalGroup>,
        Option<JournalEntry>,
    ) {
        use self::model::JournalElementReference::*;

        if let Some(reference) = reference {
            match reference {
                Character(ref name, ref entry) => (
                    JournalReferenceType::Character,
                    Some(name.into()),
                    Some(entry.into()),
                ),
                Creature(ref name, ref entry) => (
                    JournalReferenceType::Creature,
                    Some(name.into()),
                    Some(entry.into()),
                ),
                Quest(ref name, ref entry) => (
                    JournalReferenceType::Quest,
                    Some(name.into()),
                    Some(entry.into()),
                ),
                _ => (JournalReferenceType::Character, None, None),
            }
        } else {
            (JournalReferenceType::Character, None, None)
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl JournalQuestOutcomeReference {
    // ------------------------------------------------------------------------
    pub fn new(
        label_width: f32,
        options: Rc<JournalQuestList>,
        reference: Option<&model::JournalElementReference>,
    ) -> JournalQuestOutcomeReference {
        use self::model::JournalElementReference::*;

        let quest = match reference {
            Some(QuestOutcome(ref quest)) => Some(quest.into()),
            _ => None,
        };

        JournalQuestOutcomeReference {
            label_width,
            quest,
            options,
            changed: false,
            error: Ok(()),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl JournalPhaseObjectivesReference {
    // ------------------------------------------------------------------------
    pub fn new(
        label_width: f32,
        options: Rc<JournalQuestList>,
        reference: Option<&model::JournalElementReference>,
    ) -> JournalPhaseObjectivesReference {
        use self::model::JournalElementReference::*;

        let (quest, phase) = match reference {
            Some(PhaseObjectives(ref quest, ref phase)) => (Some(quest.into()), Some(phase.into())),
            _ => (None, None),
        };

        JournalPhaseObjectivesReference {
            label_width,
            quest,
            phase,
            options,
            changed: false,
            error: Ok(()),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl JournalObjectiveReference {
    // ------------------------------------------------------------------------
    pub fn new(
        label_width: f32,
        options: Rc<JournalQuestList>,
        reference: Option<&model::JournalElementReference>,
    ) -> JournalObjectiveReference {
        use self::model::JournalElementReference::*;

        let (quest, phase, objective) = match reference {
            Some(Objective(ref quest, ref phase, ref objective)) => (
                Some(quest.into()),
                Some(phase.into()),
                Some(objective.into()),
            ),
            _ => (None, None, None),
        };

        JournalObjectiveReference {
            label_width,
            quest,
            phase,
            objective,
            options,
            changed: false,
            error: Ok(()),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl JournalMapPinReference {
    // ------------------------------------------------------------------------
    pub fn new(
        label_width: f32,
        options: Rc<JournalQuestList>,
        reference: Option<&model::JournalElementReference>,
    ) -> JournalMapPinReference {
        use self::model::JournalElementReference::*;

        let (quest, phase, objective, mappin) = match reference {
            Some(Mappin(ref quest, ref phase, ref objective, ref mappin)) => (
                Some(quest.into()),
                Some(phase.into()),
                Some(objective.into()),
                Some(mappin.into()),
            ),
            _ => (None, None, None, None),
        };

        JournalMapPinReference {
            label_width,
            quest,
            phase,
            objective,
            mappin,
            options,
            changed: false,
            error: Ok(()),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// PropertyControl impls
// ----------------------------------------------------------------------------
impl PropertyControl for JournalReference {
    // ------------------------------------------------------------------------
    fn reset_changed(&mut self) {
        self.changed = false;
    }
    // ------------------------------------------------------------------------
    fn validate(&mut self) -> &mut dyn PropertyControl {
        if self.entry.is_none() {
            self.error = Err(ImString::new("must not be empty"));
        } else {
            self.error = Ok(());
        }
        self
    }
    // ------------------------------------------------------------------------
    fn process_action(&mut self, action: PropertyAction) -> Result<(), String> {
        use self::PropertyAction::*;
        self.changed = true;

        match action {
            ReferenceSetType(index) => {
                let r_type = JournalReferenceType::from(index);
                if r_type != self.r_type {
                    self.name = None;
                    self.entry = None;
                }
                self.r_type = r_type;
                self.label = ImString::new(self.r_type.as_str());
            }
            JournalSetEntry(name, entry) => {
                self.name = Some(JournalGroup::new(name));
                self.entry = Some(JournalEntry::new(entry));
            }
            _ => unreachable!(
                "reference property received unsupported action {:?}",
                action
            ),
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
    fn as_any(&self) -> &dyn Any {
        self
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl PropertyControl for JournalMapPinReference {
    // ------------------------------------------------------------------------
    fn reset_changed(&mut self) {
        self.changed = false;
    }
    // ------------------------------------------------------------------------
    fn validate(&mut self) -> &mut dyn PropertyControl {
        if self.quest.is_none() {
            self.error = Err(ImString::new("must not be empty"));
        } else {
            self.error = Ok(());
        }
        self
    }
    // ------------------------------------------------------------------------
    fn process_action(&mut self, action: PropertyAction) -> Result<(), String> {
        use self::PropertyAction::*;
        self.changed = true;

        match action {
            JournalSetMapPin(quest, phase, objective, mappin) => {
                self.quest = Some(JournalQuest::new(quest));
                self.phase = Some(JournalPhase::new(phase));
                self.objective = Some(JournalObjective::new(objective));
                self.mappin = Some(JournalMapPin::new(mappin));
            }
            _ => unreachable!(
                "reference property received unsupported action {:?}",
                action
            ),
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
    fn as_any(&self) -> &dyn Any {
        self
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl PropertyControl for JournalQuestOutcomeReference {
    // ------------------------------------------------------------------------
    fn reset_changed(&mut self) {
        self.changed = false;
    }
    // ------------------------------------------------------------------------
    fn validate(&mut self) -> &mut dyn PropertyControl {
        if self.quest.is_none() {
            self.error = Err(ImString::new("must not be empty"));
        } else {
            self.error = Ok(());
        }
        self
    }
    // ------------------------------------------------------------------------
    fn process_action(&mut self, action: PropertyAction) -> Result<(), String> {
        use self::PropertyAction::*;
        self.changed = true;

        match action {
            JournalSetQuest(quest) => {
                self.quest = Some(JournalQuest::new(quest));
            }
            _ => unreachable!(
                "reference property received unsupported action {:?}",
                action
            ),
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
    fn as_any(&self) -> &dyn Any {
        self
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl PropertyControl for JournalPhaseObjectivesReference {
    // ------------------------------------------------------------------------
    fn reset_changed(&mut self) {
        self.changed = false;
    }
    // ------------------------------------------------------------------------
    fn validate(&mut self) -> &mut dyn PropertyControl {
        if self.quest.is_none() {
            self.error = Err(ImString::new("must not be empty"));
        } else {
            self.error = Ok(());
        }
        self
    }
    // ------------------------------------------------------------------------
    fn process_action(&mut self, action: PropertyAction) -> Result<(), String> {
        use self::PropertyAction::*;
        self.changed = true;

        match action {
            JournalSetPhase(quest, phase) => {
                self.quest = Some(JournalQuest::new(quest));
                self.phase = Some(JournalPhase::new(phase));
            }
            _ => unreachable!(
                "reference property received unsupported action {:?}",
                action
            ),
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
    fn as_any(&self) -> &dyn Any {
        self
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl PropertyControl for JournalObjectiveReference {
    // ------------------------------------------------------------------------
    fn reset_changed(&mut self) {
        self.changed = false;
    }
    // ------------------------------------------------------------------------
    fn validate(&mut self) -> &mut dyn PropertyControl {
        if self.quest.is_none() {
            self.error = Err(ImString::new("must not be empty"));
        } else {
            self.error = Ok(());
        }
        self
    }
    // ------------------------------------------------------------------------
    fn process_action(&mut self, action: PropertyAction) -> Result<(), String> {
        use self::PropertyAction::*;
        self.changed = true;

        match action {
            JournalSetObjective(quest, phase, objective) => {
                self.quest = Some(JournalQuest::new(quest));
                self.phase = Some(JournalPhase::new(phase));
                self.objective = Some(JournalObjective::new(objective));
            }
            _ => unreachable!(
                "reference property received unsupported action {:?}",
                action
            ),
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
    fn as_any(&self) -> &dyn Any {
        self
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// converter
// ----------------------------------------------------------------------------
use std::convert::TryFrom;
// ----------------------------------------------------------------------------
impl TryFrom<&JournalReference> for model::JournalElementReference {
    type Error = String;
    // ------------------------------------------------------------------------
    /// no semantic checks (min str len, etc.): converts if it *can* be converted
    fn try_from(reference: &JournalReference) -> Result<Self, String> {
        use self::JournalReferenceType::*;

        let name = reference
            .name
            .as_ref()
            .map(|n| n.id().into())
            .ok_or_else(|| String::from("missing group"))?;
        let entry = reference
            .entry
            .as_ref()
            .map(|e| e.id().into())
            .ok_or_else(|| String::from("missing entry"))?;

        Ok(match reference.r_type {
            Character => model::JournalElementReference::Character(name, entry),
            Creature => model::JournalElementReference::Creature(name, entry),
            Quest => model::JournalElementReference::Quest(name, entry),
        })
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl TryFrom<&JournalQuestOutcomeReference> for model::JournalElementReference {
    type Error = String;
    // ------------------------------------------------------------------------
    /// no semantic checks (min str len, etc.): converts if it *can* be converted
    fn try_from(reference: &JournalQuestOutcomeReference) -> Result<Self, String> {
        let quest = reference
            .quest
            .as_ref()
            .map(|q| q.id().into())
            .ok_or_else(|| String::from("missing quest"))?;

        Ok(model::JournalElementReference::QuestOutcome(quest))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl TryFrom<&JournalPhaseObjectivesReference> for model::JournalElementReference {
    type Error = String;
    // ------------------------------------------------------------------------
    /// no semantic checks (min str len, etc.): converts if it *can* be converted
    fn try_from(reference: &JournalPhaseObjectivesReference) -> Result<Self, String> {
        let quest = reference
            .quest
            .as_ref()
            .map(|q| q.id().into())
            .ok_or_else(|| String::from("missing quest"))?;
        let phase = reference
            .phase
            .as_ref()
            .map(|p| p.id().into())
            .ok_or_else(|| String::from("missing phase"))?;

        Ok(model::JournalElementReference::PhaseObjectives(
            quest, phase,
        ))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl TryFrom<&JournalObjectiveReference> for model::JournalElementReference {
    type Error = String;
    // ------------------------------------------------------------------------
    /// no semantic checks (min str len, etc.): converts if it *can* be converted
    fn try_from(reference: &JournalObjectiveReference) -> Result<Self, String> {
        let quest = reference
            .quest
            .as_ref()
            .map(|q| q.id().into())
            .ok_or_else(|| String::from("missing quest"))?;
        let phase = reference
            .phase
            .as_ref()
            .map(|p| p.id().into())
            .ok_or_else(|| String::from("missing phase"))?;
        let objective = reference
            .objective
            .as_ref()
            .map(|o| o.id().into())
            .ok_or_else(|| String::from("missing objective"))?;

        Ok(model::JournalElementReference::Objective(
            quest, phase, objective,
        ))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl TryFrom<&JournalMapPinReference> for model::JournalElementReference {
    type Error = String;
    // ------------------------------------------------------------------------
    /// no semantic checks (min str len, etc.): converts if it *can* be converted
    fn try_from(reference: &JournalMapPinReference) -> Result<Self, String> {
        let quest = reference
            .quest
            .as_ref()
            .map(|q| q.id().into())
            .ok_or_else(|| String::from("missing quest"))?;
        let phase = reference
            .phase
            .as_ref()
            .map(|p| p.id().into())
            .ok_or_else(|| String::from("missing phase"))?;
        let objective = reference
            .objective
            .as_ref()
            .map(|o| o.id().into())
            .ok_or_else(|| String::from("missing objective"))?;
        let mappin = reference
            .mappin
            .as_ref()
            .map(|o| o.id().into())
            .ok_or_else(|| String::from("missing mappin"))?;

        Ok(model::JournalElementReference::Mappin(
            quest, phase, objective, mappin,
        ))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl AsStr for JournalReferenceType {
    // ------------------------------------------------------------------------
    fn as_str(&self) -> &str {
        match self {
            JournalReferenceType::Character => "character",
            JournalReferenceType::Creature => "beast",
            JournalReferenceType::Quest => "quest",
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
