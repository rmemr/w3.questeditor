//
// properties::references::scene
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
/// selectable scene reference types
#[derive(Debug, PartialEq)]
pub(in gui) enum SceneReferenceType {
    Unchecked,
    Definition,
}
// ----------------------------------------------------------------------------
/// types of interaction references
#[derive(Debug, PartialEq)]
pub(in gui) enum InteractionEntityType {
    Unchecked,
    LayerEntity,
}
// ----------------------------------------------------------------------------
pub(in gui::questgraph::blocks) struct SceneReference {
    pub(super) label_width: f32,
    pub(super) s_type: SceneReferenceType,
    pub(super) scene: input::TextField,
    pub(super) changed: bool,
}
// ----------------------------------------------------------------------------
pub(in gui::questgraph::blocks) struct InteractionReference {
    pub(super) r_type: InteractionEntityType,
    pub(super) unchecked: input::TextField,
    pub(super) world: Option<World>,
    pub(super) entity: Option<InteractionEntity>,
    pub(super) options: Rc<InteractionList>,
    pub(super) changed: bool,
    pub(super) error: Result<(), ImString>,
}
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use std::any::Any;
use std::rc::Rc;

use imgui::ImString;
use imgui_controls::input;
use imgui_controls::input::validator;
use imgui_controls::input::Field;

// actions
use super::PropertyAction;

// misc
use super::model;
use super::{AsStr, PropertyControl, Reference};

use gui::registry::{InteractionEntity, InteractionList, World};
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
impl SceneReference {
    // ------------------------------------------------------------------------
    pub fn new(width: f32, reference: Option<&model::SceneReference>) -> SceneReference {
        SceneReference {
            label_width: width,
            changed: false,
            s_type: reference.map_or(SceneReferenceType::Definition, SceneReferenceType::from),
            scene: input::TextField::new("##scene", reference.map(AsStr::as_str)).set_validators(
                vec![
                    validator::is_nonempty(),
                    validator::chars("^[0-9/._a-z]*$", "[a-z/._0-9]"),
                    validator::min_length(2),
                    validator::max_length(250),
                ],
            ),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl InteractionReference {
    // ------------------------------------------------------------------------
    fn extract_info(
        reference: Option<&model::InteractionReference>,
    ) -> (
        InteractionEntityType,
        Option<World>,
        Option<InteractionEntity>,
        Option<&str>,
    ) {
        use self::model::InteractionReference::*;

        if let Some(reference) = reference {
            match reference {
                Unchecked(tag) => (InteractionEntityType::Unchecked, None, None, Some(tag)),
                LayerEntity(ref world, ref id) => (
                    InteractionEntityType::LayerEntity,
                    Some(World::new(world)),
                    Some(InteractionEntity::new(id)),
                    None,
                ),
            }
        } else {
            (InteractionEntityType::Unchecked, None, None, None)
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// PropertyControl impls
// ----------------------------------------------------------------------------
impl PropertyControl for SceneReference {
    // ------------------------------------------------------------------------
    fn reset_changed(&mut self) {
        self.changed = false;
        self.scene.reset_changed();
    }
    // ------------------------------------------------------------------------
    fn validate(&mut self) -> &mut dyn PropertyControl {
        if self.scene.validate().valid() {
            // additional check if scene ref matches type constraints
            let scene = self.scene.value().as_str().unwrap();
            if let Some(errmsg) = match self.s_type {
                SceneReferenceType::Definition if scene.starts_with('/') => {
                    Some("absolute paths for scene definitions not supported.")
                }
                SceneReferenceType::Definition if scene.ends_with(".w2scene") => {
                    Some("scene definition name must not end with .w2scene")
                }
                SceneReferenceType::Unchecked if !scene.ends_with(".w2scene") => {
                    Some("w2scene filepath must end with .w2scene")
                }
                _ => None,
            } {
                self.scene.set_error_msg(errmsg.to_owned());
            }
        }
        self
    }
    // ------------------------------------------------------------------------
    fn process_action(&mut self, action: PropertyAction) -> Result<(), String> {
        use self::PropertyAction::*;
        self.changed = true;

        match action {
            ReferenceSetType(index) => {
                self.s_type = SceneReferenceType::from(index);
            }
            Update(ref value) => {
                self.scene.validate().set_value(value.into())?;
            }
            _ => unreachable!(
                "reference property received unsupported action {:?}",
                action
            ),
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
    fn as_any(&self) -> &dyn Any {
        self
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl PropertyControl for InteractionReference {
    // ------------------------------------------------------------------------
    fn reset_changed(&mut self) {
        self.unchecked.reset_changed();
        self.changed = false;
    }
    // ------------------------------------------------------------------------
    fn validate(&mut self) -> &mut dyn PropertyControl {
        match self.r_type {
            InteractionEntityType::Unchecked => {
                self.unchecked.validate();
            }
            _ if self.entity.is_none() => {
                self.error = Err(ImString::new("must not be empty"));
            }
            _ => {
                self.error = Ok(());
            }
        }
        self
    }
    // ------------------------------------------------------------------------
    fn process_action(&mut self, action: PropertyAction) -> Result<(), String> {
        use self::PropertyAction::*;
        self.changed = true;

        match action {
            ReferenceSetInteractionEntityType(new_type) => {
                let r_type = new_type;
                if r_type != self.r_type {
                    self.world = None;
                    self.entity = None;
                    self.unchecked.reset();
                }
                self.r_type = r_type;
            }
            ReferenceSetInteractionEntity(world, entityid) => {
                self.world = Some(World::new(world));
                self.entity = Some(InteractionEntity::new(entityid));
            }
            Update(ref value) => {
                self.unchecked.validate().set_value(value.into())?;
            }
            _ => unreachable!(
                "reference property received unsupported action {:?}",
                action
            ),
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
    fn as_any(&self) -> &dyn Any {
        self
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// wrappable references
// ----------------------------------------------------------------------------
impl Reference for InteractionReference {
    // ------------------------------------------------------------------------
    fn error(&self) -> Result<&(), &ImString> {
        match self.r_type {
            InteractionEntityType::Unchecked => self.unchecked.error(),
            _ => self.error.as_ref(),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// converter
// ----------------------------------------------------------------------------
impl From<&model::SceneReference> for SceneReferenceType {
    // ------------------------------------------------------------------------
    fn from(reference: &model::SceneReference) -> SceneReferenceType {
        use self::model::SceneReference::*;

        match reference {
            Unchecked(_) => SceneReferenceType::Unchecked,
            Definition(_) => SceneReferenceType::Definition,
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'model>
    From<(
        bool,
        Rc<InteractionList>,
        Option<&'model model::InteractionReference>,
    )> for InteractionReference
{
    // ------------------------------------------------------------------------
    fn from(
        data: (
            bool,
            Rc<InteractionList>,
            Option<&'model model::InteractionReference>,
        ),
    ) -> InteractionReference {
        let (required, interactive_entities, reference) = data;

        let (r_type, world, entity, unchecked) = Self::extract_info(reference);

        let mut validators = vec![
            validator::chars("^[0-9_a-zA-Z]*$", "[a-zA-Z_0-9]"),
            validator::min_length(2),
            validator::max_length(250),
        ];

        if required {
            validators.push(validator::is_nonempty());
        }

        InteractionReference {
            r_type,
            unchecked: input::TextField::new("##unchecked", unchecked).set_validators(validators),
            world,
            entity,
            options: interactive_entities,
            changed: false,
            error: Ok(()),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
use std::convert::TryFrom;

impl TryFrom<&SceneReference> for model::SceneReference {
    type Error = String;
    // ------------------------------------------------------------------------
    /// no semantic checks (min str len, etc.): converts if it *can* be converted
    fn try_from(reference: &SceneReference) -> Result<Self, String> {
        use self::SceneReferenceType::*;

        let scene = match reference.scene.value().as_str()? {
            s if s.ends_with(".yml") => s.split_at(s.len() - 4).0.to_lowercase(),
            s => s.to_owned(),
        };

        Ok(match reference.s_type {
            Unchecked => model::SceneReference::Unchecked(scene),
            Definition => model::SceneReference::Definition(scene),
        })
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl TryFrom<&InteractionReference> for model::InteractionReference {
    type Error = String;
    // ------------------------------------------------------------------------
    /// no semantic checks (min str len, etc.): converts if it *can* be converted
    fn try_from(reference: &InteractionReference) -> Result<Self, String> {
        use self::InteractionEntityType::*;

        if let Unchecked = reference.r_type {
            let tag = reference
                .unchecked
                .value()
                .as_str()
                .unwrap_or("")
                .to_owned();
            Ok(model::InteractionReference::Unchecked(tag))
        } else {
            let world = reference
                .world
                .as_ref()
                .map(|w| w.id().into())
                .unwrap_or("")
                .to_owned();
            let entity = reference
                .entity
                .as_ref()
                .map(|t| t.id().into())
                .unwrap_or("")
                .to_owned();

            Ok(match reference.r_type {
                LayerEntity => model::InteractionReference::LayerEntity(world, entity),
                Unchecked => unreachable!(),
            })
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl AsStr for model::SceneReference {
    // ------------------------------------------------------------------------
    fn as_str(&self) -> &str {
        use self::model::SceneReference::*;

        match self {
            Unchecked(ref value) | Definition(ref value) => value.as_str(),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
