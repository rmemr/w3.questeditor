//
// properties::update
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
#[inline]
pub(super) fn handle_action(
    id: &PropertyId,
    action: PropertyAction,
    state: &mut PropertyEditorState,
) -> Result<(), String> {
    handle_property_action(id, action, state)?;
    Ok(())
}
// ----------------------------------------------------------------------------

// state
use super::PropertyEditorState;

// actions
use super::PropertyAction;

// misc
use super::{Property, PropertyId};

// ----------------------------------------------------------------------------
// action groups
// ----------------------------------------------------------------------------
fn handle_property_action(
    id: &PropertyId,
    action: PropertyAction,
    state: &mut PropertyEditorState,
) -> Result<(), String> {
    use self::Property::*;
    use self::PropertyAction::*;

    if let Some(property) = state.properties.iter_mut().find(|p| p.id() == id.as_str()) {
        match property {
            Field(_, field) => match action {
                Update(ref value) => field.validate().set_value(value.into())?,
                _ => unreachable!("property field action != update action"),
            },
            Control(_, control) => {
                // invert order of validate and process_action since some actions
                // may putcontrol into a valid or invalid action which has to be
                // updated directly and not with next action (which may follow much later)
                control.process_action(action)?;
                control.validate();
            }
        }
    } else {
        error!(
            "action for unknown propertyfield {}: {:?}",
            id.as_str(),
            action
        );
    }

    Ok(())
}
// ----------------------------------------------------------------------------
