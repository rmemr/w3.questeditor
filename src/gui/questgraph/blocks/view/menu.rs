//
// blocks::view::context menus
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
pub(in gui::questgraph) fn draw_block_context_menu(
    ui: &Ui<'_>,
    questblock: &dyn QuestBlock,
    graphblock: &GraphBlock,
) -> Option<MenuAction> {
    match graphblock.id() {
        BlockId::Scene(_) | BlockId::Interaction(_) => {
            draw_scene_block_context_menu(ui, graphblock)
        }
        BlockId::Script(_) => draw_script_block_context_menu(ui, graphblock),
        BlockId::Randomize(_) => draw_randomize_block_context_menu(ui, graphblock),
        BlockId::WaitUntil(_) => draw_waituntil_block_context_menu(ui, questblock, graphblock),
        _ => None,
    }
    .map(MenuAction::from)
}
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use imgui::Ui;

// actions
use super::{BlockAction, MenuAction};

// state

// misc
use super::EditorBlockHint;
use super::MAX_SOCKETS;
use super::{BlockId, GraphBlock, QuestBlock};
// ----------------------------------------------------------------------------
fn draw_scene_block_context_menu(ui: &Ui<'_>, block: &GraphBlock) -> Option<BlockAction> {
    let mut result = None;

    ui.menu(im_str!("add socket...")).build(|| {
        if ui
            .menu_item(im_str!("in-socket"))
            .enabled(block.in_sockets().len() < MAX_SOCKETS)
            .build()
        {
            result = Some(BlockAction::OnAddInSocket(block.id().to_owned()));
        }
        if ui
            .menu_item(im_str!("out-socket"))
            .enabled(block.out_sockets().len() < MAX_SOCKETS)
            .build()
        {
            result = Some(BlockAction::OnAddOutSocket(block.id().to_owned()));
        }
    });

    ui.menu(im_str!("delete socket...")).build(|| {
        for socket in block.in_sockets() {
            if ui
                .menu_item(socket.as_ref())
                // if default socket is the last socket don't allow to delete it
                // it would be recreated anyway but potentially existing edge would be
                // removed
                .enabled(block.in_sockets().len() > 1 || socket != "Input")
                .build()
            {
                result = Some(BlockAction::DeleteInSocket(
                    block.id().to_owned(),
                    socket.into(),
                ));
            }
        }

        ui.separator();
        for socket in block.out_sockets() {
            if ui
                .menu_item(socket.as_ref())
                // if default socket is the last socket it don't allow to delete it
                // it would be recreated anyway but potentially existing edge would be
                // removed
                .enabled(block.out_sockets().len() > 1 || socket != "Out")
                .build()
            {
                result = Some(BlockAction::DeleteOutSocket(
                    block.id().to_owned(),
                    socket.into(),
                ));
            }
        }
    });

    ui.separator();

    result
}
// ----------------------------------------------------------------------------
fn draw_randomize_block_context_menu(ui: &Ui<'_>, block: &GraphBlock) -> Option<BlockAction> {
    let mut result = if ui
        .menu_item(im_str!("add out-socket"))
        .enabled(block.out_sockets().len() < MAX_SOCKETS)
        .build()
    {
        Some(BlockAction::OnAddOutSocket(block.id().to_owned()))
    } else {
        None
    };

    ui.menu(im_str!("delete socket...")).build(|| {
        for socket in block.out_sockets() {
            if ui
                .menu_item(socket.as_ref())
                // if default socket is the last socket it don't allow to delete it
                // it would be recreated anyway but potentially existing edge would be
                // removed
                .enabled(block.out_sockets().len() > 2 || socket != "Out")
                .build()
            {
                result = Some(BlockAction::DeleteOutSocket(
                    block.id().to_owned(),
                    socket.into(),
                ));
            }
        }
    });

    ui.separator();

    result
}
// ----------------------------------------------------------------------------
#[inline]
fn draw_script_block_context_menu(ui: &Ui<'_>, block: &GraphBlock) -> Option<BlockAction> {
    let mut result = None;

    let is_branching = block.out_sockets().len() > 1;
    let mut branch = is_branching;
    let mut ignore = !is_branching;

    if ui
        .menu_item(im_str!("ignore result"))
        .selected(&mut ignore)
        .build()
        && is_branching
    {
        result = Some(BlockAction::ScriptResultBranch(
            block.id().to_owned(),
            false,
        ));
    }

    if ui
        .menu_item(im_str!("branch on result"))
        .selected(&mut branch)
        .build()
        && !is_branching
    {
        result = Some(BlockAction::ScriptResultBranch(block.id().to_owned(), true));
    }
    ui.separator();

    result
}
// ----------------------------------------------------------------------------
#[inline]
fn draw_waituntil_block_context_menu(
    ui: &Ui<'_>,
    questblock: &dyn QuestBlock,
    block: &GraphBlock,
) -> Option<BlockAction> {
    let mut result = None;

    let mut has_named_conditions = block.out_sockets().len() > 1;

    if let Some(Some(hint)) = questblock.editordata().map(|data| &data.hint) {
        has_named_conditions = has_named_conditions || EditorBlockHint::NamedConditions == *hint;
    }

    if has_named_conditions {
        if ui
            .menu_item(im_str!("add out-socket"))
            .enabled(block.out_sockets().len() < MAX_SOCKETS)
            .build()
        {
            result = Some(BlockAction::OnAddOutSocket(block.id().to_owned()));
        }

        ui.menu(im_str!("delete out-socket...")).build(|| {
            for socket in block.out_sockets() {
                if ui
                    .menu_item(socket.as_ref())
                    // if default socket is the last socket it don't allow to delete it
                    // it would be recreated anyway but potentially existing edge would be
                    // removed
                    .enabled(block.out_sockets().len() > 1 || socket != "Out")
                    .build()
                {
                    result = Some(BlockAction::DeleteOutSocket(
                        block.id().to_owned(),
                        socket.into(),
                    ));
                }
            }
        });
        ui.separator();
    }

    result
}
// ----------------------------------------------------------------------------
