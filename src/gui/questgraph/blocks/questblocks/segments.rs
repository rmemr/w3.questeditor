//
// blocks::questblocks::segments
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use std::convert::TryFrom;

use model::questgraph::{QuestBlock, SegmentId};
use model::questgraph::segments;

use super::properties;
use super::properties::Property;
use super::properties::SegmentReference;
use super::properties::view::LABEL_WIDTH;

use super::ListProvider;
use super::EditableBlock;
// ----------------------------------------------------------------------------
// EditableBlock trait impl
// ----------------------------------------------------------------------------
impl EditableBlock for segments::QuestStart {
    // ------------------------------------------------------------------------
    fn properties(&self, _listprovider: &ListProvider) -> Vec<Property> {
        Vec::default()
    }
    // ------------------------------------------------------------------------
    fn set_properties(&mut self, _properties: &[Property]) -> Result<(), String> {
        Ok(())
    }
    // ------------------------------------------------------------------------
    #[inline]
    fn as_questblock(&self) -> &dyn QuestBlock {
        self
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl EditableBlock for segments::QuestEnd {
    // ------------------------------------------------------------------------
    fn properties(&self, _listprovider: &ListProvider) -> Vec<Property> {
        Vec::default()
    }
    // ------------------------------------------------------------------------
    fn set_properties(&mut self, _properties: &[Property]) -> Result<(), String> {
        Ok(())
    }
    // ------------------------------------------------------------------------
    #[inline]
    fn as_questblock(&self) -> &dyn QuestBlock {
        self
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl EditableBlock for segments::SegmentIn {
    // ------------------------------------------------------------------------
    fn properties(&self, _listprovider: &ListProvider) -> Vec<Property> {
        Vec::default()
    }
    // ------------------------------------------------------------------------
    fn set_properties(&mut self, _properties: &[Property]) -> Result<(), String> {
        Ok(())
    }
    // ------------------------------------------------------------------------
    #[inline]
    fn as_questblock(&self) -> &dyn QuestBlock {
        self
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl EditableBlock for segments::SegmentOut {
    // ------------------------------------------------------------------------
    fn properties(&self, _listprovider: &ListProvider) -> Vec<Property> {
        Vec::default()
    }
    // ------------------------------------------------------------------------
    fn set_properties(&mut self, _properties: &[Property]) -> Result<(), String> {
        Ok(())
    }
    // ------------------------------------------------------------------------
    #[inline]
    fn as_questblock(&self) -> &dyn QuestBlock {
        self
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl EditableBlock for segments::SubSegment {
    // ------------------------------------------------------------------------
    fn properties(&self, listprovider: &ListProvider) -> Vec<Property> {
        vec![
            Property::new_control(
                "##subsegment",
                Box::new(properties::LabeledReference::<SegmentReference>::new(
                    LABEL_WIDTH,
                    "subsegment",
                    listprovider.filtered_segments(),
                    true,
                    self.segmentlink(),
                )),
            ),
        ]
    }
    // ------------------------------------------------------------------------
    fn set_properties(&mut self, properties: &[Property]) -> Result<(), String> {
        for property in properties {
            match property {
                Property::Control(id, ctl) if id.as_str() == "##subsegment" => {
                    let segment = ctl_to_property!(
                        ctl,
                        properties::LabeledReference<SegmentReference>,
                        "##subsegment"
                    );

                    self.set_opt_segmentlink(SegmentId::try_from(segment.reference()).ok());
                }
                unknown => super::unknown_property(self.uid(), unknown.id())?,
            }
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
    #[inline]
    fn as_questblock(&self) -> &dyn QuestBlock {
        self
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
