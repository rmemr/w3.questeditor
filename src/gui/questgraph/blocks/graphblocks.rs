//
// blocks::graphblocks functions on questblock display objects in graphs
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
pub(in gui::questgraph) trait EditableBlock {
    /// sorts, dedupes and attaches list of sockets to block
    fn set_sockets(
        &mut self,
        in_sockets: Vec<InSocketId>,
        out_sockets: Vec<OutSocketId>,
    ) -> Result<(), String>;
    /// attaches block specific default sockets if block doesn't have any in- or any out-sockets
    fn add_missing_sockets(&mut self);
    fn set_validation_state(&mut self, validation_state: BlockValidationState);
}
// ----------------------------------------------------------------------------
/// creates a new graphblock with tooltip and in/out sockets
pub(super) fn create_new(
    id: &BlockId,
    pos: Option<(f32, f32)>,
    validation_state: BlockValidationState,
    sockets: Option<(Vec<InSocketId>, Vec<OutSocketId>)>,
) -> Result<GraphBlock, String> {
    // graph knows about source and sink types for auto layout reasoning
    let block_type = match id {
        BlockId::QuestStart(_) => graph::BlockType::Source,
        BlockId::QuestEnd(_) => graph::BlockType::Sink,
        BlockId::SegmentIn(_) => graph::BlockType::Source,
        BlockId::SegmentOut(_) => graph::BlockType::Sink,
        _ => graph::BlockType::Other,
    };

    let mut block = graph::Block::new(
        block_type,
        id.clone(),
        id.name().replace('_', "\n"),
    );

    if let Some(pos) = pos {
        block.set_position(pos);
    }
    block.set_validation_state(validation_state);

    if let Some((in_sockets, out_sockets)) = sockets {
        block.set_sockets(in_sockets, out_sockets)?;
    }

    Ok(block)
}
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use imgui_controls::graph;

use gui::questgraph::{Block as GraphBlock, BlockId, InSocketId, OutSocketId};

use super::view;

use super::BlockValidationState;
use super::MAX_SOCKETS;
// ----------------------------------------------------------------------------
impl EditableBlock for GraphBlock {
    // ------------------------------------------------------------------------
    fn set_sockets(
        &mut self,
        mut in_sockets: Vec<InSocketId>,
        mut out_sockets: Vec<OutSocketId>,
    ) -> Result<(), String> {
        in_sockets.sort();
        in_sockets.dedup();
        out_sockets.sort();
        out_sockets.dedup();

        if in_sockets.len() <= MAX_SOCKETS && out_sockets.len() <= MAX_SOCKETS {
            self.set_in_sockets(in_sockets);
            self.set_out_sockets(out_sockets);
            Ok(())
        } else {
            Err(format!(
                "{}: maximum number of supported in/out sockets on block is {}. failed to create sockets.",
                self.id(),
                MAX_SOCKETS
            ))
        }
    }
    // ------------------------------------------------------------------------
    fn add_missing_sockets(&mut self) {
        use model::questgraph::primitives;

        // some blocks have always the same sockets which need to overwrite
        let (set_in_sockets, set_out_sockets) = if primitives::has_static_sockets(self.id()) {
            (true, true)
        } else {
            (self.in_sockets().is_empty(), self.out_sockets().is_empty())
        };

        if set_in_sockets || set_out_sockets {
            let sockets = primitives::get_default_sockets(self.id());

            if set_in_sockets {
                self.set_in_sockets(sockets.0.iter().map(|s| s.into()).collect());
            }
            if set_out_sockets {
                self.set_out_sockets(sockets.1.iter().map(|s| s.into()).collect());
            }
        }
    }
    // ------------------------------------------------------------------------
    fn set_validation_state(&mut self, validation_state: BlockValidationState) {
        let is_valid = validation_state.is_ok();

        let mut tooltip = vec![format!(
            "type: {}\nname: {}",
            self.id().type_caption(),
            self.id().name().replace('_', " ")
        )];

        if let Err(error) = validation_state {
            tooltip.push(error);
        };
        let view_properties = view::properties_for_blocktype(self.id(), is_valid);

        self.set_tooltip(tooltip)
            .set_draw_properties(view_properties);
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
