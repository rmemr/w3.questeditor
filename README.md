# Witcher 3 Questgraph Editor

A questgraph editor for "The Witcher 3: Wild Hunt" game by CD Projekt Red, part of radish community modding tools.

![Example Screenshot](example.screenshot.png)

radish modding tools are a collection of community created modding tools aimed to enable the creation of new quests for "The Witcher 3: Wild Hunt" game by CDPR.

The full package will be available here: https://www.nexusmods.com/witcher3/mods/3620

## Building from Source

The project can be compiled with the `stable` rust-toolchain version 1.36 or higher. Windows platform requires the MSVC toolchain.

1. Clone with all submodules:
   ```sh
   $ git clone --recurse-submodules https://codeberg.org/rmemr/w3.questeditor.git
   ```

2. The environment variables `GIT_HASH` and `BUILD_TIME` must be set prior building, e.g. in linux
   ```sh
   $ export GIT_HASH=dev-build; export BUILD_TIME=dev-build;
   ```
   or in windows commandline:
   ```sh
   > set GIT_HASH=dev-build & set BUILD_TIME=dev-build
   ```
   or in windows powershell:
   ```sh
   > [Environment]::SetEnvironmentVariable("GIT_HASH", "dev-build") 
   > [Environment]::SetEnvironmentVariable("BUILD_TIME", "dev-build") 
   ```

3. Build with:
   ```sh
   > cargo build
   ```
   or as release build
   ```sh
   > cargo build --release
   ```

## Starting

The quest definitions loaded by the editor require some static definitions (e.g information about area hubs). These are stored in the "repo.quests" directory. The editor tries to load the definitions on startup from the "repo.quests" directory by default. You can provide another location the parameter --repo-dir <path-to-repo> where <path-to-repo> can be a relative or absolute path to the "repo.quests" content dir.


```shell
  cargo run
```

Example build debug-mode version and start with cargo (windows):
```sh
   > set GIT_HASH=dev-build & set BUILD_TIME=dev-build & cargo run
```

## Options

```shell
> w3-quest-editor --help
w3 quest editor v0.1.12 (git:dev-build build:dev-build)
Usage: w3-quest-editor [options]

Options:
    -h, --help          print this help menu
    -d, --data-dir DIRECTORY
                        defines directory of quest definitions to edit
    -r, --repo-dir DIRECTORY
                        defines directory of repository for required quest
                        independent definitions. default is repo.quests/
    -v, --verbose       show debug messages in console
        --very-verbose  show more debug messages in console
```

## Contributing

First: thank you for your interest! There are many ways to contribute. You can write bug reports, create pull requests to fix bugs or add new features or write documentation.

Please make sure your pull requests:
  * reference a ticket, if you want to add a new feature please make a ticket first and outline what you are trying to add
  * is formatted with rustfmt
  * compiles with the current master branch

If you have questions, you can find me on the [nexusmods discord server][nexusmods-discord] in the #the-witcher-3 channel.

[nexusmods-discord]:       https://discord.gg/tJDuqw5
